import java.util.*;

public class Client {
	private final int id;
	private final int arrivingTime;
	private int processingTime;
	private int waitingTime = 0;
	
	public Client(int id, int arrivingTime, int processingTime) {
		this.id = id;
		this.arrivingTime = arrivingTime;
		this.processingTime = processingTime;
	}
	
	public static List<Client> generateClients(Settings settings) {
		var clients = new ArrayList<Client>();
		for (int i = 0; i < settings.getClientNumber(); i++) {
			clients.add(
					new Client(
							i + 1,
							new Random().nextInt(settings.getMaxArrivingTime() - settings.getMinArrivingTime()) + settings.getMinArrivingTime(),
							new Random().nextInt(settings.getMaxProcessingTime() - settings.getMinProcessingTime()) + settings.getMinProcessingTime()
					)
			);
		}
		clients.sort(Comparator.comparingInt(Client::getArrivingTime));
		return clients;
	}
	
	public void passSecond() {
		waitingTime++;
	}
	
	public void process() {
		processingTime--;
	}
	
	public int getArrivingTime() {
		return arrivingTime;
	}
	
	public int getProcessingTime() {
		return processingTime;
	}
	
	public int getWaitingTime() {
		return waitingTime;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Client)) return false;
		Client client = (Client) o;
		return id == client.id &&
				getArrivingTime() == client.getArrivingTime() &&
				getProcessingTime() == client.getProcessingTime() &&
				getWaitingTime() == client.getWaitingTime();
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id, getArrivingTime(), getProcessingTime(), getWaitingTime());
	}
	
	@Override
	public String toString() {
		return "(" + id + ", " + arrivingTime + ", " + processingTime + ") ";
	}
}
