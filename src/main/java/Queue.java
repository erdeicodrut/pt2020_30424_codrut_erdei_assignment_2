import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Queue {
	private final List<Client> clients;
	private int totalWaitingTime = 0;
	private int numberOfClients = 0;
	private boolean run;
	
	public Queue() {
		clients = new ArrayList<>();
		run = false;
	}
	
	public void addClient(Client client) {
		clients.add(client);
	}
	
	public void passSecond() {
		clients.forEach(Client::passSecond);
		
		clients.get(0).process();
		
		if (clients.get(0).getProcessingTime() == 0) {
			totalWaitingTime += clients.get(0).getWaitingTime();
			numberOfClients++;
			clients.remove(clients.get(0));
		}
		
		run = false;
	}
	
	public int getWaitingTime() {
		int waitingTime = 0;
		for (Client client : clients) {
			waitingTime += client.getProcessingTime();
		}
		return waitingTime;
	}
	
	public int getNumberOfClients() {
		return numberOfClients;
	}
	
	public int getTotalWaitingTime() {
		return totalWaitingTime;
	}
	
	public boolean isEmpty() {
		return clients.isEmpty();
	}
	
	@Override
	public String toString() {
		return Arrays.toString(clients.toArray());
	}
	
	public boolean shouldRun() {
		return run;
	}
	
	public void runOnce() {
		run = true;
	}
}
