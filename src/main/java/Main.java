import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

public class Main {
	static final int second = 1000;
	
	static void to_run(Queue queue) {
		for (; ; ) {
			if (!queue.shouldRun()) {
				try {
					Thread.sleep(1);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				continue;
			}
			queue.passSecond();
			if (queue.isEmpty()) {
				return;
			}
		}
	}
	
	static HashMap<Queue, Thread> queues = new HashMap<>();
	
	static void dispatch(Client client) {
		Queue q = queues.keySet().stream().min((a, b) -> a.getWaitingTime() - b.getTotalWaitingTime()).get();
		if (q.isEmpty()) {
			queues.put(q, new Thread(() -> to_run(q)));
			queues.get(q).start();
		}
		q.addClient(client);
	}
	
	public static void main(String[] args) {
		var settings = new Settings(args[0]);
		var printingQueue = new LinkedList<String>();
		var clients = Client.generateClients(settings);
		for (int i = 0; i < settings.getQueueNumber(); i++) {
			var q = new Queue();
			queues.put(q, null);
		}
		var dispatcher = new Thread(() -> {
			int simulationTime;
			for (
					simulationTime = 0;
					simulationTime <= settings.getSimulationTime() &&
							(queues.values().stream().anyMatch((it) -> it != null && it.isAlive()) ||
									!clients.isEmpty());
					simulationTime++
			) {
				for (Queue queue : queues.keySet()) {
					if (queues.get(queue) != null && !queues.get(queue).isAlive()) {
						queues.put(queue, null);
					}
				}
				
				final int currentTime = simulationTime;
				var toDispatch = clients
						.stream()
						.filter((client) -> client.getArrivingTime() == currentTime);
				clients
						.stream()
						.filter((client) -> client.getArrivingTime() == currentTime)
						.forEach(Main::dispatch);
				clients.removeAll(toDispatch.collect(Collectors.toList()));
				printStatus(printingQueue, clients, simulationTime);
				for (Queue queue1 : queues.keySet()) {
					if (!queue1.isEmpty()) {
						queue1.runOnce();
					}
				}
				try {
					Thread.sleep(second);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (simulationTime <= settings.getSimulationTime()) {
				printStatus(printingQueue, clients, simulationTime);
			}
		});
		
		dispatcher.start();
		
		try {
			dispatcher.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		queues.values().stream().filter(Objects::nonNull).forEach(Thread::stop);
		
		int totalTime = 0;
		int totalClients = 0;
		
		for (Queue queue : queues.keySet()) {
			totalTime += queue.getTotalWaitingTime();
			totalClients += queue.getNumberOfClients();
		}
		
		printingQueue.add(
				"Average time: " + totalTime / totalClients + "\n"
		);
		
		FileWriter file;
		try {
			file = new FileWriter(new File(args[1]));
			for (String s : printingQueue) {
				file.write(s + "\n");
			}
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void printStatus(LinkedList<String> printingQueue, List<Client> clients, int simulationTime) {
		StringBuilder stringBuilder = new StringBuilder();
		int i = 0;
		
		for (Queue queue : queues.keySet()) {
			stringBuilder
					.append("Queue ")
					.append(++i)
					.append(": ")
					.append(queue.isEmpty() ? "Closed" : queue)
					.append("\n");
		}
		
		printingQueue.add(
				"Time: " + simulationTime + "\nWaiting list: " + Arrays.toString(clients.toArray()) + "\n" + stringBuilder.toString()
		);
		
		
	}
}