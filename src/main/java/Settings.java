import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Settings {
	private final int clientNumber;
	private final int queueNumber;
	private final int simulationTime;
	private final int minArrivingTime;
	private final int maxArrivingTime;
	private final int minProcessingTime;
	private final int maxProcessingTime;
	
	public Settings(String fileName) {
		Scanner file = null;
		try {
			file = new Scanner(Files.readString(Paths.get(fileName).toAbsolutePath()).replace(",", " "));
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		clientNumber = file.nextInt();
		queueNumber = file.nextInt();
		simulationTime = file.nextInt();
		minArrivingTime = file.nextInt();
		maxArrivingTime = file.nextInt();
		minProcessingTime = file.nextInt();
		maxProcessingTime = file.nextInt();
	}
	
	public int getClientNumber() {
		return clientNumber;
	}
	
	public int getQueueNumber() {
		return queueNumber;
	}
	
	public int getSimulationTime() {
		return simulationTime;
	}
	
	public int getMinArrivingTime() {
		return minArrivingTime;
	}
	
	public int getMaxArrivingTime() {
		return maxArrivingTime;
	}
	
	public int getMinProcessingTime() {
		return minProcessingTime;
	}
	
	public int getMaxProcessingTime() {
		return maxProcessingTime;
	}
}
